/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.examen2t1.models;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author Sistemas-21
 */
public class Empleado implements Serializable{
    private String Name;
    private String SecondName;
    private String lastName;
    private String SecondlastName;
    private Date birthDate;
    private int age;

    public String getName() {
        return Name;
    }

    public void setName(String Name) {
        this.Name = Name;
    }

    public String getSecondName() {
        return SecondName;
    }

    public void setSecondName(String SecondName) {
        this.SecondName = SecondName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSecondlastName() {
        return SecondlastName;
    }

    public void setSecondlastName(String SecondlastName) {
        this.SecondlastName = SecondlastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    
}
