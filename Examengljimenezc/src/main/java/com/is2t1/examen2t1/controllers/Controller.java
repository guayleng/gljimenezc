/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.is2t1.examen2t1.controllers;

import com.is2t1.examen2t1.viewa.EmployeeFrame;
import com.is2t1.examen2t1.viewa.MainFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Sistemas-21
 */
public class Controller implements ActionListener{
    MainFrame mainFrame;
    
    public Controller(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }
    
    
    @Override
    public void actionPerformed(ActionEvent event) {
        switch(event.getActionCommand()){
            case "empleado":
                showEmployeeFrame();
                break;
            case "exit":
                System.exit(0);
                break;
        }
        
    }
    
    private void showEmployeeFrame(){
        EmployeeFrame pf = new EmployeeFrame();
        mainFrame.showChild(pf, false);
    }
    

}
